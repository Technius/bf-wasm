{-# LANGUAGE FlexibleContexts #-}

module Lib
    ( eval
    , evalString
    , Context
    , Symbol
    ) where

import Control.Monad (foldM, liftM)
import Data.Char (chr)
import Data.Vector ((!), (//), Vector)
import Control.Monad.Writer
import qualified Data.Vector as Vector
import Text.Parsec
import Text.Read (readMaybe)

data Symbol = MoveLeft
            | MoveRight
            | Increment
            | Decrement
            | Repeat [Symbol]
            | Output
            | Input
            deriving (Show, Eq)

data Context = MkContext { ptr :: Int
                         , cells :: Vector Int
                         }

valueAt :: Int -> Context -> Int
valueAt p ctx = (cells ctx) ! p

valueAtPtr :: Context -> Int
valueAtPtr ctx = valueAt (ptr ctx) ctx

defaultContext :: Context
defaultContext = MkContext { ptr = 0, cells = Vector.replicate 1000 0 }

modifyCellAtPtr :: (Int -> Int) -> Context -> Context
modifyCellAtPtr f ctx =
  let p = ptr ctx
      values = cells ctx
      value = values ! p
  in ctx { cells = values // [(p, f value)] }

type EvalResult a = WriterT String IO a

evalMany :: [Symbol] -> Context -> EvalResult Context
evalMany symbs ctx = foldM (\ctx' next -> eval next ctx') ctx symbs

eval :: Symbol -> Context -> EvalResult Context
eval MoveLeft ctx = pure $ ctx { ptr = (ptr ctx) - 1 }
eval MoveRight ctx = pure $ ctx { ptr = (ptr ctx) + 1 }
eval Increment ctx = pure $ modifyCellAtPtr (+ 1) ctx
eval Decrement ctx = pure $ modifyCellAtPtr (+ (-1)) ctx
eval Input ctx = lift $ fmap (\x -> modifyCellAtPtr (const x) ctx) inputLoop
  where inputLoop :: IO Int
        inputLoop = do
          inputStr <- getLine
          case readMaybe inputStr of
            Just x -> pure x
            _ -> inputLoop
eval Output ctx = writer (ctx, [chr ((cells ctx) ! (ptr ctx))])
eval (Repeat symbs) ctx =
  if valueAtPtr ctx == 0
    then pure ctx
    else evalMany symbs ctx >>= eval (Repeat symbs)

grammar :: (Stream s m Char) => ParsecT s () m [Symbol]
grammar = many1 oneSymbol

loop :: (Stream s m Char) => ParsecT s () m Symbol
loop = do
  char '['
  symbs <- grammar
  char ']'
  pure (Repeat symbs)

oneSymbol :: (Stream s m Char) => ParsecT s () m Symbol
oneSymbol = incOp <|> decOp <|> movelOp <|> moverOp <|> outOp <|> inOp <|> loop
  where
    incOp = char '+' >> pure Increment
    decOp = char '-' >> pure Decrement
    movelOp = char '<' >> pure MoveLeft
    moverOp = char '>' >> pure MoveRight
    outOp = char '.' >> pure Output
    inOp = char ',' >> pure Input

evalString :: String -> IO ()
evalString str =
  case parse grammar "" str of
    Right symbs -> do
      (_, out) <- runWriterT $ evalMany symbs defaultContext
      putStrLn out
    Left err -> print err
